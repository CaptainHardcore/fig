package com.itkindaworks.figure;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by remember me on 13.05.2017.
 */

public class Result extends AppCompatActivity {
    private static int currentLat2;
    private static int currentLon2;
    private static int latitudedot2 ;
    private static int longlitudedot2 ;
    private Location mLocation2;


    private LocationManager locationManager2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        locationManager2 = (LocationManager) getSystemService(LOCATION_SERVICE);
        latitudedot2 = Integer.parseInt(getIntent().getStringExtra("LAT"));
        Log.i("STEP", String.valueOf(latitudedot2));
        longlitudedot2 = Integer.parseInt(getIntent().getStringExtra("LON"));
        Log.i("STEP", String.valueOf(longlitudedot2));
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager2.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000, 1, locationListener2);
        locationManager2.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 3000, 2,
                locationListener2);


    }

    private LocationListener locationListener2 = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            currentLon2 = (int) (100000 * location.getLongitude());
            Log.i("STEP", "Curent "+String.valueOf(currentLon2));
            currentLat2 = (int) (100000 * location.getLatitude());
            Log.i("STEP", "Current "+String.valueOf(currentLat2));
            Log.i("STEP", "CHanged");
            Intent intent2 = new Intent(Result.this, MainActivity.class);
            if (currentLat2 > latitudedot2 + 50 || currentLat2 < latitudedot2 - 50 ||
                    currentLon2 > longlitudedot2 + 50 || currentLon2 < longlitudedot2 - 50
                    ) {
                locationManager2.removeUpdates(locationListener2);
                Log.i("VALUES", "GET Acess started");


                intent2.putExtra("LAT2", latitudedot2);
                intent2.putExtra("LON2", longlitudedot2);
                intent2.putExtra("FAIL", true);
                Log.i("STEP", "STARTED");
                startActivity(intent2);
                Log.i("STEP", "IS FAILED");


                Toast.makeText(Result.this, "EAH", Toast.LENGTH_SHORT).show();
                Log.i("VALUES", "START ACTIVITy");
            }else intent2.putExtra("FAIL", false);

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }


    };

}