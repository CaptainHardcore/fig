package com.itkindaworks.figure;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    private DatabaseReference mSimpleFirechatDatabaseReference;
    private FirebaseRecyclerAdapter<ChatMessage, FirechatMsgViewHolder>
            mFirebaseAdapter;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private Button mSendButton;
    private EditText mMsgEditText;

    private SharedPreferences mSharedPreferences;
    public static final String DEFAULT_NAME = "aleksander";
    private String mUsername;
    private String mPhotoUrl;
    private LocationManager locationManager;
    private static int latitudedot = 0;
    private static int longlitudedot = 0;
    private Location mLocation;
    private boolean isFailed = false;



    private static int currentLat;
    private static int currentLon;
    private String oleg = "false";
    private String sss = "false";
    private String sanya = "false";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSimpleFirechatDatabaseReference.child("messages").removeValue();
    }

    public static class FirechatMsgViewHolder extends RecyclerView.ViewHolder {





        public TextView msgTextView;
        public TextView userTextView;
        public CircleImageView userImageView;

        public FirechatMsgViewHolder(View v) {
            super(v);
            msgTextView = (TextView) itemView.findViewById(R.id.msgTextView);
            userTextView = (TextView) itemView.findViewById(R.id.userTextView);
            userImageView = (CircleImageView) itemView.findViewById(R.id.userImageView);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isFailed = Boolean.parseBoolean(getIntent().getStringExtra("FAIL"));
        Log.i("STEP","is failed"+ String.valueOf(isFailed));

        if (isFailed){
            latitudedot = Integer.parseInt(getIntent().getStringExtra("LAT2"));
            longlitudedot = Integer.parseInt(getIntent().getStringExtra("LON2"));
            mSendButton.setEnabled(false);
        }



        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mUsername = DEFAULT_NAME;
        


        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);

        mSimpleFirechatDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessage,
                FirechatMsgViewHolder>(
                ChatMessage.class,
                R.layout.chat_message,
                FirechatMsgViewHolder.class,
                mSimpleFirechatDatabaseReference.child("messages")) {

            @Override
            protected void populateViewHolder(FirechatMsgViewHolder viewHolder, ChatMessage friendlyMessage, int position) {
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                viewHolder.msgTextView.setText(friendlyMessage.getText());
                viewHolder.userTextView.setText(friendlyMessage.getName());

                if (friendlyMessage.getName().equals("SANYA")){
                    if (friendlyMessage.getText().equals("true")){
                        sanya = "true";
                        Log.i("STEP", "OKAY");
                    }
                }
                if (friendlyMessage.getName().equals("OLEG")){
                    if (friendlyMessage.getText().equals("true")){
                        oleg = "true";
                        Log.i("STEP", "OKAY");
                    }
                }


                if (friendlyMessage.getPhotoUrl() == null) {
                    viewHolder.userImageView
                            .setImageDrawable(ContextCompat
                                    .getDrawable(MainActivity.this,
                                            R.mipmap.ic_launcher_round));
                } else {
                    Glide.with(MainActivity.this)
                            .load(friendlyMessage.getPhotoUrl())
                            .into(viewHolder.userImageView);
                }
            }
        };

        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int chatMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition =
                        mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (chatMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });

        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(mFirebaseAdapter);
    }
    @Override
    protected void onResume() {
        super.onResume();



        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000, 3, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 3000, 3,
                locationListener);
        checkEnabled();

    }
    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
            mLocation = location;
            currentLon = (int)(100000*location.getLongitude());
            currentLat = (int)(100000*location.getLatitude());
         if (currentLat<latitudedot+20 && currentLat>latitudedot-20&&
                    currentLon<longlitudedot+20&& currentLon>longlitudedot-20){
                Log.i("STE", "GET Acess");
            //

              sss = "true";
             Log.i("STE", "SSS TRUE");}
         else sss = "false";
            Log.i("STEP", "SSS FALSE");
             if( sanya.equals("true")&&oleg.equals("true")){
                 locationManager.removeUpdates(locationListener);
                 Intent intent = new Intent(getApplicationContext(), Result.class);
             intent.putExtra("LAT", String.valueOf(latitudedot));
             intent.putExtra("LON", String.valueOf(longlitudedot));
                 mSimpleFirechatDatabaseReference.child("messages").removeValue();
                startActivity(intent);


                Toast.makeText(getApplicationContext(), "EAH", Toast.LENGTH_SHORT).show();
                Log.i("VALUES", "START ACTIVITy");}



        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            showLocation(locationManager.getLastKnownLocation(provider));
        }


        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {

            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {

            }
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {

            ChatMessage chatMessage  = new ChatMessage(sss, "SANYA", "photo");
            mSimpleFirechatDatabaseReference.child("messages")
                    .push().setValue(chatMessage);
            Log.i("BASE", "Message Pushed");

        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
        }
    }



    private void checkEnabled() {

    }
    public void onGps(View view){
        double a = mLocation.getLongitude();
        longlitudedot = (int) (100000.0 *a);
        Log.i("VALUES", String.valueOf(longlitudedot));
        latitudedot = (int)(100000*mLocation.getLatitude());
        Log.i("VALUES", String.valueOf(latitudedot));

    }
}
